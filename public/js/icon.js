function act() {

//$(".ic#ICONE").html(SVG_CODE);

$(".ic#heart").html('<svg xmlns="http://www.w3.org/2000/svg" role="img" aria-hidden="true" viewBox="0 0 512 512" focusable="false" data-prefix="fal" data-icon="heart"><path fill="currentColor" d="M 458.4 64.3 C 400.6 15.7 311.3 23 256 79.3 C 200.7 23 111.4 15.6 53.6 64.3 C -21.6 127.6 -10.6 230.8 43 285.5 l 175.4 178.7 c 10 10.2 23.4 15.9 37.6 15.9 c 14.3 0 27.6 -5.6 37.6 -15.8 L 469 285.6 c 53.5 -54.7 64.7 -157.9 -10.6 -221.3 Z m -23.6 187.5 L 259.4 430.5 c -2.4 2.4 -4.4 2.4 -6.8 0 L 77.2 251.8 c -36.5 -37.2 -43.9 -107.6 7.3 -150.7 c 38.9 -32.7 98.9 -27.8 136.5 10.5 l 35 35.7 l 35 -35.7 c 37.8 -38.5 97.8 -43.2 136.5 -10.6 c 51.1 43.1 43.5 113.9 7.3 150.8 Z" /></svg>');



$(".ic#heart-fill").html('<svg xmlns="http://www.w3.org/2000/svg" role="img" aria-hidden="true" viewBox="0 0 512 512" focusable="false" data-prefix="fas" data-icon="heart"><path class="" fill="currentColor" d="M 462.3 62.6 C 407.5 15.9 326 24.3 275.7 76.2 L 256 96.5 l -19.7 -20.3 C 186.1 24.3 104.5 15.9 49.7 62.6 c -62.8 53.6 -66.1 149.8 -9.9 207.9 l 193.5 199.8 c 12.5 12.9 32.8 12.9 45.3 0 l 193.5 -199.8 c 56.3 -58.1 53 -154.3 -9.8 -207.9 Z" /></svg>');

}



$('.fl').click(function(event) {

  var id=$(this).prop('id');
  if(id.includes("-fill")) {

    idAfter = id.replace("-fill", "");
    $(this).attr("id", idAfter);
    act();

  } else {

    id+="-fill";
    $(this).attr("id", id);
    act();

  }
})




$(act());

<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

	<meta name="keywords" content="quentin, sar">
	<meta name="description" content="Dans une démarche éco-citoyenne, nous aimerions partager vos idées afin d'améliorer le quotidien des gens mais également changer les habitudes des plus grosse entreprises.">
	<meta name="author" content="Quentin Sar, sarquentin.fr, Spileur, Iqhawe">
	<meta name="reply-to" content="contact@sarquentin.fr">
	<meta name='subject' content="subject_empty">
	<meta name='language' content='FR'>
	<meta name='owner' content='Quentin Sar'>
	<meta name='url' content='url_empty'>
	<meta name='identifier-URL' content='url_empty'>
	<meta name='target' content='all'>
	<meta name="theme-color" content="#35BF54">

	<link rel='shortcut icon' type='image/ico' href='/public/images/logo.png'>
	<link rel='logo' type='image/png' href='/public/images/logo.png'>

	<meta property="og:title" content="My EcoIdea" />
	<meta property="og:description" content="Dans une démarche éco-citoyenne, nous aimerions partager vos idées afin d'améliorer le quotidien des gens mais également changer les habitudes des plus grosse entreprises." />
	<meta property="og:image" content="https://my-ecoidea.sarquentin.fr/public/images/logo.png" />
	<meta property="og:site_name" content="My EcoIdea" />
	<meta property="og:type" content="website" />
	<meta property="og:locale" content="fr_FR" />

	<meta name="twitter:card" content="summary" />
	<meta name="twitter:site" content="@K_Dev_" />
	<meta name="twitter:title" content="My EcoIdea" />
	<meta name="twitter:description" content="Dans une démarche éco-citoyenne, nous aimerions partager vos idées afin d'améliorer le quotidien des gens mais également changer les habitudes des plus grosse entreprises." />
	<meta name="twitter:image" content="/public/images/logo.png" />

	<title>My EcoIdea</title>

	<meta http-equiv="content-language" content="fr">

  <link rel="stylesheet" type="text/css" href="/public/stylesheets/index.css">
  <link rel="stylesheet" type="text/css" href="/public/stylesheets/pages/idea_master.css">
</head>
<body>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/public/apps/header.html') ?>
	<main>
    <div id="search-box">
      <input id="search-txt" class="left" placeholder="Rechercher par mot clé">
      <a class="right" id="search-btn">
        <svg  role="img" aria-hidden="true" viewBox="0 0 512 512" focusable="false" data-prefix="far" data-icon="search"><path fill="currentColor" d="M 508.5 468.9 L 387.1 347.5 c -2.3 -2.3 -5.3 -3.5 -8.5 -3.5 h -13.2 c 31.5 -36.5 50.6 -84 50.6 -136 C 416 93.1 322.9 0 208 0 S 0 93.1 0 208 s 93.1 208 208 208 c 52 0 99.5 -19.1 136 -50.6 v 13.2 c 0 3.2 1.3 6.2 3.5 8.5 l 121.4 121.4 c 4.7 4.7 12.3 4.7 17 0 l 22.6 -22.6 c 4.7 -4.7 4.7 -12.3 0 -17 Z M 208 368 c -88.4 0 -160 -71.6 -160 -160 S 119.6 48 208 48 s 160 71.6 160 160 s -71.6 160 -160 160 Z" /></svg>
      </a>
    </div>
    <div id="ideas-container"><!-- container Idées-->
    <div class="idea"><!-- Idée -->
      <div class="header"><!-- Head -->
        <img src="https://sarquentin.fr/src/img/logo.png" class="authorLogo" alt="{autorLogo}">
        <span class="authorName">Quentin Sar</span>
      </div>
      <div class="content"><!-- Preview -->
        <span>Remplacer les tickets de caisse papier par des ticket virtuel (email/sms)</span>
      </div>
      <div class="action"><!-- Div d'action -->
        <input type="button" href="{idea_link}" class="button discover" value="Découvrir">
        <div id="heart" class="ic medium fl"></div><p>3909</p>
      </div>
    </div>
    <div class="idea"><!-- idée-->
      <div class="header">
        <img src="/public/images/iqhawe.jpg" class="authorLogo" alt="{autorLogo}">
        <span class="authorName">Iqhawe</span>
      </div>
      <div class="content">
        <span>Amener ses propres poches en papier au supermarché, boycotter les emballages.</span>
      </div>
      <div class="action">
        <input type="button" href="{idea_link}" class="button discover" value="Découvrir">
        <div id="heart" class="ic medium fl"></div><p>{idea_like}</p>
      </div>
    </div><!-- End idée -->
    <div class="idea"><!-- idée-->
      <div class="header">
        <img src="https://via.placeholder.com/50" class="authorLogo" alt="{autorLogo}">
        <span class="authorName">Spileur</span>
      </div>
      <div class="content">
        <span>Ton idée?</span>
      </div>
      <div class="action">
        <input type="button" href="{idea_link}" class="button discover" value="Découvrir">
        <div id="heart" class="ic medium fl"></div><p>{idea_like}</p>
      </div>
    </div><!-- End idée -->
    <div class="idea"><!-- idée-->
      <div class="header">
        <img src="https://via.placeholder.com/50" class="authorLogo" alt="{autorLogo}">
        <span class="authorName">Célia</span>
      </div>
      <div class="content">
        <span>Pour les distributeurs, amener ses propres bouteilles/gourdes à remplir</span>
      </div>
      <div class="action">
        <input type="button" href="{idea_link}" class="button discover" value="Découvrir">
        <div id="heart" class="ic medium fl"></div><p>120</p>
      </div>
    </div><!-- End idée -->
    </div><!-- End container idées-->
	</main>
  <?php include($_SERVER['DOCUMENT_ROOT'].'/public/apps/menu.html') ?>
</body>
<script src="/public/js/icon.js"></script>
</html>

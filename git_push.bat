@echo off
git add *
git log --max-count=1 
echo #
echo #DERNIERE VERSION CI-DESSUS
echo #
echo #Petites informations :
echo # - Lors d'un push mettez la version suivante
echo # - Lors d'un push, si vous avez uniquement ajoute un fichier vide, rajouter un + a la version actuelle
echo #
echo # developpe avec amour par Quentin
echo #
set /p COMMENT=Veuillez entrer le nom de la version (pour le commit) : 
echo #
echo #
echo #
git commit -m "%COMMENT%"
git push
pause
<!DOCTYPE html>
<html lang="fr">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="theme-color" content="#35BF54">

  <link rel='shortcut icon' type='image/ico' href='/public/images/logo.png'>
  <link rel='logo' type='image/png' href='/public/images/logo.png'>

	<title>My EcoIdea | Mon idée</title>


  <link rel="stylesheet" type="text/css" href="/public/stylesheets/pages/idea_new.css">
</head>
<body>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/public/apps/header.html') ?>
	<main >
    <div class="form">
      <h2>Partager son idée</h2>
      <form method="post">
        <p>Les mots clé permettent de classer et rechercher les idées :</p>
        <p><input name="keyword1" placeholder="Mot-clé n°1" required><input placeholder="Mot-clé n°2" name="keyword2" required><input placeholder="Mot-clé n°3" name="keyword3" required></p>
        <br><p><h3>Preview de l'idée :</h3></p>
        <p><textarea name="preview" rows="4" maxlength="200" required placeholder="C'est l'aperçu de l'idée : le texte qui définit l'idée dans ses grandes lignes. Soyez court mais précis"></textarea></p>
        <br>
        <p><h3>Description complète : </h3></p>
        <textarea name="desc" style="resize: vertical;" rows="20" required placeholder="Projet de grande envergure? Vous avez la place de vous exprimer ! Abordez l'idée sous tout ses angles.."></textarea></p>
        <p><input id="anonyme" type="checkbox"><label for="anonyme" >Anonyme</label></p>
        <br>
        <input type="submit" value="Partager">
      </form>
  	</div>
	</main>
  <?php include($_SERVER['DOCUMENT_ROOT'].'/public/apps/menu.html') ?>
</body>
</html>

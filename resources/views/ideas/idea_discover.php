<!DOCTYPE html>
<?php
$idea_content="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
$idea_id="{{idea_id}}";
?>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="theme-color" content="#35BF54">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My EcoIdea | Idée</title>

    <link rel='shortcut icon' type='image/ico' href='/public/images/logo.png'>
  	<link rel='logo' type='image/png' href='/public/images/logo.png'>

    <meta http-equiv="content-language" content="fr">

  	<link rel="stylesheet" type="text/css" href="/public/stylesheets/pages/idea_discover.css">

  </head>
  <body>
    <?php include($_SERVER['DOCUMENT_ROOT'].'/public/apps/header.html') ?>
    <main>
      <div class="idea">
      <div class="user-info">
        <img src="https://via.placeholder.com/50">
        <p id="user_name">Quentin Sar</p>
        <p id="post_date">posté le 18/03/2019</p>
      </div>
        <div class="idea_header">
          <h3>Entreprise, Ticket de caisse, Papier</h3>
        </div>
        <div class="idea_content">
          <p><?php echo $idea_content; ?></p>
        </div>

        <div class="idea_info"><div id="heart" class="ic little fl"></div><i id="likes">15 J'aime</i><i id="adhesions">2 Adhésions</i></div>
      </div>
      <input id="adherer" type="button" name="adherer" value="Adhérer">
      <div class="share">
        <h3>Partager cette idée</h3>
        <?php include($_SERVER['DOCUMENT_ROOT'].'/public/apps/share.php'); ?>
      </div>
    </main>
    <?php include($_SERVER['DOCUMENT_ROOT'].'/public/apps/menu.html') ?>
    <script src="/public/js/icon.js">
    </script>
  </body>
</html>

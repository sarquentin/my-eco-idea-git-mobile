<!DOCTYPE html>
<html lang="fr">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="theme-color" content="#35BF54">

  <link rel='shortcut icon' type='image/ico' href='/public/images/logo.png'>
  <link rel='logo' type='image/png' href='/public/images/logo.png'>

	<title>My EcoIdea | Inscription</title>

  <script src="/public/js/jquery-3.3.1.min.js"></script>

  <link rel="stylesheet" type="text/css" href="/public/stylesheets/pages/auth.css">
	<link rel="stylesheet" type="text/css" href="/public/stylesheets/master.css">
</head>
<body>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/public/apps/header.html') ?>
	<main>
    <div class="register main">
      <form class="feed" method="POST" action="{{ route('register') }}">
        <h2>S'inscrire</h2>
        <input class="flow" name="pseudo" id="pseudo" placeholder="Pseudo" required>
        <input class="flow" name="email" id="email" type="email" placeholder="Email" required>
        <input class="flow" name="password" type="password" placeholder="Mot de passe" required>
        <input class="flow" type="password" placeholder="Confirmer le mot de passe" required>
        <p><input type="checkbox" id="cguConfirm" required><label for="cguConfirm">J'accepte les <a href="/cgu/">CGU</a></label></p>
        <input class="flow" id="submit" type="submit" value="Inscription">
        <p><a href="{{ route('login') }}">Se connecter</a></p>
      </form>
    </div>
	</main>
  <?php include($_SERVER['DOCUMENT_ROOT'].'/public/apps/menu.html') ?>
</body>
</html>

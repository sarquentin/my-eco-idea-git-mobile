<!DOCTYPE html>
<html lang="fr">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="theme-color" content="#35BF54">

  <link rel='shortcut icon' type='image/ico' href='/public/images/logo.png'>
  <link rel='logo' type='image/png' href='/public/images/logo.png'>

	<title>My EcoIdea | Connexion</title>

  <script src="/public/js/jquery-3.3.1.min.js"></script>

  <link rel="stylesheet" type="text/css" href="/public/stylesheets/pages/auth.css">
	<link rel="stylesheet" type="text/css" href="/public/stylesheets/master.css">
</head>
<body>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/public/apps/header.html') ?>
	<main>
    <div class="login main">
      <form class="feed" method="get" action="{{ route('login') }}">
        <h2>Se connecter</h2>
        <input class="flow" name="email" id="email" type="email" value="{{ old('email') }}" placeholder="Email">
        <input class="flow" name="password" id="password" type="password" placeholder="Mot de passe">
        <p><input name="remember" type="checkbox" id="remember" {{ old('remember') ? 'checked' : '' }}><label for="remember">Se souvenir de moi</label></p>
        <input class="flow" id="submit" type="submit" value="Connexion">
        <p><a href="{{ route('register') }}">S'inscrire</a></p>
      </form>
    </div>
	</main>
  <?php include($_SERVER['DOCUMENT_ROOT'].'/public/apps/menu.html') ?>
</body>
</html>

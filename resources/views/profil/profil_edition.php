<!DOCTYPE html>
<html lang="fr">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="theme-color" content="#35BF54">

  <link rel='shortcut icon' type='image/ico' href='/public/images/logo.png'>
  <link rel='logo' type='image/png' href='/public/images/logo.png'>

	<title>My EcoIdea | Mon idée</title>

  <script src="/public/js/jquery-3.3.1.min.js"></script>

  <link rel="stylesheet" type="text/css" href="/public/stylesheets/pages/profil_styles.css">
	<link rel="stylesheet" type="text/css" href="/public/stylesheets/master.css">
</head>
<body>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/public/apps/header.html') ?>
	<main >
    <div class="main">
      <form class="feed" method="get" action="{{ route('modify_profile') }}">
        <h2>Modifier son profil</h2>
        <img height="150" alt="test Avatar" src="/public/images/logo.png">
        <label for="avatarFile">Changer d'avatar<input name="avatar" class="disable" id="avatarFile" aria-describedby="fileHelp" type="file" accept="image/png, image/jpeg"></label>
        <p>
          <h4>Pseudo</h4>
          <input class="flow" name="pseudo" id="pseudo" value="{{ old('pseudo') }}" placeholder="Pseudo" required>
          <h4>Email</h4>
          <input class="flow" name="email" id="email" type="email" value="{{ old('email') }}" placeholder="Email" required>
        </p>
        <p>
          <h4>Mot de passe (laisser tel quel si inchangé)</h4>
          <input class="flow" name="password" id="password" type="password" placeholder="Mot de passe" value="{{ old('password') }}" required>
          <br>
          <input class="flow" type="password" placeholder="Confirmer le mot de passe" value="{{ old('password') }}" required>
        </p>
        <p><input class="bottom" id="save" type="submit" value="Enregistrer"></p>
      </form>
    </div>
	</main>
  <?php include($_SERVER['DOCUMENT_ROOT'].'/public/apps/menu.html') ?>
</body>
</html>

<!DOCTYPE html>
<html lang="fr">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="theme-color" content="#35BF54">

  <link rel='shortcut icon' type='image/ico' href='/public/images/logo.png'>
  <link rel='logo' type='image/png' href='/public/images/logo.png'>

	<title>My EcoIdea | Mon idée</title>

  <script src="/public/js/jquery-3.3.1.min.js"></script>

  <link rel="stylesheet" type="text/css" href="/public/stylesheets/pages/profil_styles.css">
	<link rel="stylesheet" type="text/css" href="/public/stylesheets/master.css">
</head>
<body>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/public/apps/header.html') ?>
	<main >
    <div class="main">
      <img height="150" alt="test Avatar" src="/public/images/logo.png">
      <h2>{user_name}</h2>
      <span>Inscrit le {date_inscription}</span>
      <p>Idée(s) posté(s) : {nb_idea}</p>
      <!-- SI C'EST MON PROFIL -> édition, historique idées liké et adhérées -->
      <div class="action">
        <a href="/resources/views/profil/profil_edition.php"> <input type="button" value="Modifier le profil"></a>
        <p>
          <a><input class="light" type="button" value="Mes idées"></a>
          <a><input class="light" type="button" value="Idées likés"></a>
          <a><input class="light" type="button" value="Idées que j'adhère"></a>
        </p>
      </div>
    </div>
	</main>
  <?php include($_SERVER['DOCUMENT_ROOT'].'/public/apps/menu.html') ?>
</body>
</html>

<!DOCTYPE html>
<html lang="fr">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

	<meta name="keywords" content="quentin, sar">
	<meta name="description" content="De nombreux mouvements internationaux ont fait surface afin de préserver notre environnement. Des associations, des groupes de discussions, des marches, des défis… Aujourd’hui, nous nous basons sur une idée venant de Twitter : Publier des idées pour améliorer notre éco-citoyenneté. De nos jours, nous assistons une prise de conscience importante de la population pour l’écologie.">
	<meta name="author" content="Quentin Sar, sarquentin.fr, Spileur, Iqhwe">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="reply-to" content="contact@sarquentin.fr">
	<meta name='subject' content="subject_empty">
	<meta name='language' content='FR'>
	<meta name='owner' content='Quentin Sar'>
	<meta name='url' content='url_empty'>
	<meta name='identifier-URL' content='url_empty'>
	<meta name='target' content='all'>
	<meta name="theme-color" content="#35BF54">

	<link rel='shortcut icon' type='image/png' href='/mobile/public/images/logo.png'>
	<link rel='logo' type='image/png' href='/mobile/public/images/logo.png'>

	<meta property="og:title" content="My EcoIdea - Quel est notre concept ?" />
	<meta property="og:description" content="De nombreux mouvements internationaux ont fait surface afin de préserver notre environnement. Des associations, des groupes de discussions, des marches, des défis… Aujourd’hui, nous nous basons sur une idée venant de Twitter : Publier des idées pour améliorer notre éco-citoyenneté. De nos jours, nous assistons une prise de conscience importante de la population pour l’écologie." />
	<meta property="og:image" content="image_empty" />
	<meta property="og:site_name" content="My EcoIdea - Quel est notre concept ?" />
	<meta property="og:type" content="website" />
	<meta property="og:video" content="video_empty" />
	<meta property="og:locale" content="fr_FR" />

	<meta name="twitter:card" content="summary" />
	<meta name="twitter:site" content="@K_Dev_" />
	<meta name="twitter:title" content="My EcoIdea - Quel est notre concept ?" />
	<meta name="twitter:description" content="De nombreux mouvements internationaux ont fait surface afin de préserver notre environnement. Des associations, des groupes de discussions, des marches, des défis… Aujourd’hui, nous nous basons sur une idée venant de Twitter : Publier des idées pour améliorer notre éco-citoyenneté. De nos jours, nous assistons une prise de conscience importante de la population pour l’écologie." />
	<meta name="twitter:image" content="/public/images/logo.png" />

	<title>My EcoIdea - Quel est notre concept ?</title>

	<meta http-equiv="content-language" content="fr">

	<link rel="stylesheet" type="text/css" href="/mobile/public/stylesheets/concept.css">
</head>
<body>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/public/apps/header.html') ?>
  <main>
  	<img class="feuille" src="/mobile/public/images/feuille.png">
  	<img class="feuille f2" src="/mobile/public/images/feuille.png">
  	<img class="feuille f3" src="/mobile/public/images/feuille.png">
  	<img class="feuille f4" src="/mobile/public/images/feuille.png">
  	<div class="plante p1"></div>
  	<div class="plante_rervese p2"></div>
  	<div class="plante p3"></div>
  	<div class="plante_rervese p4"></div>
  	<div class="projet-informations">
  		<div id="id1">
  			<div class="box content-right">
  				<h3 id="sub_title">Une idée populaire</h3>
  				<p id="text">De nombreux mouvements internationaux ont fait surface afin de préserver notre environnement. Des associations, des groupes de discussions, des marches, des défis… Aujourd’hui, nous nous basons sur une idée venant de Twitter : Publier des idées pour améliorer notre éco-citoyenneté. De nos jours, nous assistons une prise de conscience importante de la population pour l’écologie.</p>
  			</div>
  		</div>
  		<div id="id2">
  			<div class="box content-left">
	  			<h3 id="sub_title">Une démarche écologique</h3>
	  			<p id="text">Avant toutes choses, nous désirons adapter une démarche écologique à tout point de vue. Nous travaillons avec l’aide de nombreux acteurs de l’écologie afin de respecter au maximum la planète. Malgré le fait qu’un data-center (lieu ou est stocké le site) ne soit pas forcément écologique, nous tentons de favoriser une limitation de la consommation électrique en diminuant les requêtes.</p>
	  		</div>
  		</div>
  		<div id="id3">
  			<div class="box content-right">
	  			<h3 id="sub_title">L'éco-citoyenneté</h3>
	  			<p id="text">Nous vous avons parlé de l’éco-citoyenneté, mais qu’est ce que c’est ? Selon le dictionnaire : c’est le comportement d’une personne ou d’un groupe de personnes qui repose sur le respect des règles et des principes visant à respecter l’environnement. Cela peut se traduire par des actions de tous les jours, mais également une nouvelle approche des entreprises et des structures face à l’écologie.</p>
	  		</div>
  		</div>
	  	<div id="id1">
	  		<div class="box content-left">
		  		<h3 id="sub_title">Comment utiliser My Eco Idea ?</h3>
		  		<p id="text">My EcoIdea est un projet regroupant vos idées pour l'écologie. Dans une démarche éco-citoyenne, nous aimerions partager vos idées afin d'améliorer le quotidien des gens, mais également changer les habitudes des entreprises.
	Un titre, une description, et votre idée devient publique.
	<i>- Partagez</i> vos idées au monde entier.
	<i>- Partagez</i> les idées que vous aimez.
	<i>- Likez</i> les idées qui vous plaisent.
	<i>- Adhérez</i> aux idées, recevez les actualités et les notifications de celles-ci.</p>
				</div>
	  	</div>
  	</div>
  </main>
  <?php include($_SERVER['DOCUMENT_ROOT'].'/public/apps/menu.html') ?>
</body>
</html>
